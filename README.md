# Cloud_Project_5
By Jiayi Zhou
[![pipeline status](https://gitlab.com/JiayiZhou36/cloud_project_5/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/cloud_project_5/-/commits/main)

## Purpose of Project
This project creates serverless rust microservice thatprocess string data which removes leading and trailing whitespace in a string and return a formatted string.

## Requirements
* Create a Rust AWS Lambda function (or app runner)
* Implement a simple service--sentence trimming
* Connect to a database--connect to DynamoDB in AWS

## Database with function
![Screenshot_2024-02-29_at_4.24.08_PM](/uploads/84626e5cd99b02fcb76d8ae3d490beda/Screenshot_2024-02-29_at_4.24.08_PM.png)
